# eslint-config-aligent #


### What is this repository for? ###

Defining javascript linting rules for aligent projects.

### How do I get set up? ###

1. Add eslint-config-aligent to your package.json deps: 

```
	...
    "@aligent/eslint-config": "^2.1.3",
	...
```

2. add `.eslintrc.json` to your project root

```
{
  "extends": "@aligent/eslint-config"
}
```

### Contribution guidelines ###

* Code review

### Who do I talk to? ###

* Frontend Team