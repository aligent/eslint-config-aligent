# Changelog

## v2.4.1

- Update packages and default airbnb base rules / react rules
- Disable requiring jsDoc on class definitions due to bug.

## v2.4.0

- Add jsDoc plugin
- Add jsDoc recommended rules
- Add jsDoc custom rules
- Add `no-extra-parens` rule
- Add `function-paren-newline` rule to be consistent
- Add `no-param-reassign` rule
- Detect react version
- Bump ecmaVersion to 2019
- Remove eslint-plugin-security recommended rules

## v2.3.0

- Rename to @aligent/eslint-config due to required eslint plugin rules

## v2.2.0

- Rename to @aligent/eslint-preset

## v2.1.5

- Packages audit update

## v2.1.4

- First npm publish
- Added licensing

## v2.1.3

- Added eslint-plugin-security plugin
- Added eslint-plugin-security recommended rules

## v2.1.2

- Integrated with Bitbucket Pipelines
- Update Eslint 5.12.1

## v2.1.1

- Added `import/extensions` rules
- Turned off `import/no-extraneous-dependencies`

## v2.1.0

- Added JSX-ally plugins
- Added JSX-ally recommended rules

## v2.0.0

- Made into aligent plugin

## v1.0.0

- Initial release
