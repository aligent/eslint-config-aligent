module.exports = {
    plugins: [
        'requirejs',
        'react',
        'jest',
        'jsx-a11y',
        'security',
        'jsdoc'
    ],
    extends: [
        'airbnb-base',
        'plugin:react/recommended',
        'plugin:jsx-a11y/recommended',
        'plugin:jsdoc/recommended'
    ],
    env: {
        es6: true,
        browser: true,
        amd: true,
        'jest/globals': true
    },
    parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion: 2019,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
            generators: true,
            experimentalObjectRestSpread: true
        }
    },
    settings: {
        react: {
            createClass: 'createReactClass', // Regex for Component Factory to use,
            // default to 'createReactClass'
            pragma: 'React', // Pragma to use, default to 'React'
            version: 'detect', // React version, default to the latest React stable release
            propWrapperFunctions: ['forbidExtraProps'] // The names of any functions used to wrap the
            // propTypes object, e.g. `forbidExtraProps`.
            // If this isn't set, any propTypes wrapped in
            // a function will be skipped.
        }
    },
    rules: {
        // Allow AMD require/define
        'import/no-amd': 'off',

        // Update line indenting to 4 spaces
        indent: ['error', 4, { SwitchCase: 1 }],

        // Incrase maximum length of a single line
        'max-len': ['error', 160, 2],

        // Enforce the location of arrow function bodies with implicit returns - too strong so OFF
        'implicit-arrow-linebreak': 'off',

        // Require padding within classes, disallow everywhere else
        'padded-blocks': ['error', { blocks: 'never', classes: 'always', switches: 'never' }],

        // Allow multi spaces for lining up variable declarations and properties
        'no-multi-spaces': ['error', { exceptions: { VariableDeclarator: true, Property: true } }],

        // Allow leading/trailing underscors when used with this keyword - allowing for 'privately' named functions/params
        'no-underscore-dangle': ['warn', { allowAfterThis: true }],

        // Don't require trailing comma for last elements in arrays
        'comma-dangle': ['error', 'never'],

        // Disable forcing single line arrow functions
        'arrow-body-style': 'off',

        // Disable forcing the use of `this` inside class methods
        'class-methods-use-this': 'off',

        // Disable extensions on all js type files
        'import/extensions': ['error', 'never', { json: 'always', scss: 'always', css: 'always' }],

        // Disable check due to false positives with React component imports
        'import/no-unresolved': 'off',

        // Disable warning importing from devDeps
        'import/no-extraneous-dependencies': 'off',

        camelcase: ['warn', { properties: 'never', ignoreDestructuring: true }],

        // This rule restricts the use of parentheses to only where they are necessary.
        'no-extra-parens': ['error', 'all', { ignoreJSX: 'all', returnAssign: false }],

        // Assignment to variables declared as function parameters can be misleading and lead to confusing behavior
        'no-param-reassign': ['error', { props: false }],

        // This rule enforces consistent line breaks inside parentheses of function parameters or arguments.
        'function-paren-newline': ['error', 'consistent'],

        // Dont require JSDoc return descriptions
        'jsdoc/require-returns-description': 0,

        // Checks for presence of jsdoc comments, on class declarations as well as functions.
        'jsdoc/require-jsdoc': [
            'error',
            {
                require: {
                    ClassDeclaration: false,
                    FunctionDeclaration: true,
                    MethodDefinition: false
                },
                exemptEmptyFunctions: true
            }
        ]
    }
};
