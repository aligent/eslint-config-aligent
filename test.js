import React from 'react';
import child_process from 'child_process';

/**
 * @function testFunction
 *
 * @param {string} name Description
 *
 * @returns {string}
 */
function testFunction(name) {
    return name;
}

export const person = {
    first_name: 'john',

    getName() {
        return this.first_name;
    }
};

testFunction();
/**
 *
 * @param {string} num A description
 *
 * @returns {React.ReactElement}
 */
function testJsx(num) {
    if (num !== '') {
        return person[num];
    }
    return <div>Hello</div>;
}

// jsDoc should not flag empty functions
function doSomething() {}

doSomething();

const path = 'user input';
child_process.exec(`ls -l ${path}`, (err, data) => {
    console.log(data);
});

testJsx();

/**
 * @class Person
 */
export class Person {

    constructor() {
        this.name = 'john smith';
    }

}

const p = new Person();

p.name = 'jane';
